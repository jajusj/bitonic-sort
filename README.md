# Bitonic sort

[Bitonic sort](https://en.wikipedia.org/wiki/Bitonic_sorter) is a 
parallel sort which works well with GPUs. 

This is a simple program based on `impBitonicSort` function from 
https://courses.cs.duke.edu//fall08/cps196.1/Pthreads/bitonic.c. 

# Compilation
```bash
nvcc -O3 -o bitonic bitonic.cu
```

# Execution
```bash
time ./bitonic
```

# Limitations
1. Number of elements can only be in power of 2. 
2. No shared memory and/or other optimisations are not used. 
