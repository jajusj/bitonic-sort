#include <cstdlib>
#include <cuda.h>
#include <cuda_runtime.h>
#include <iomanip>
#include <iostream>
#include <ostream>
#include <stdexcept>
#include <vector>

template<typename T>
std::ostream&
operator << (std::ostream& t_os, const std::vector<T>& t_elems)
{
  if (t_elems.size())
  {
    t_os<<"[";
    for (std::size_t e = 0; e < t_elems.size() - 1; ++e)
    {
      t_os<<t_elems[e]<<", ";
    }
    t_os<<t_elems[t_elems.size() - 1];
    t_os<<"]";
  }
  
  return t_os;
}

__global__
void
complete_one_pass_of_bitonic_sort_on_device (const std::size_t t_h, 
  const std::size_t t_n, 
  int* t_A)
{
  const auto idx = blockDim.x * blockIdx.x + threadIdx.x;
  const auto next_idx = idx ^ t_n;

  if (next_idx > idx)
  {
    if (idx & t_h) /// 0: top half, 1: bottom half.
    {
      if (t_A[idx] < t_A[next_idx])
      {
        const auto tmp = t_A[idx];
        t_A[idx] = t_A[next_idx];
        t_A[next_idx] = tmp;
      }
    }
    else
    {
      if (t_A[idx] > t_A[next_idx])
      {
        const auto tmp = t_A[idx];
        t_A[idx] = t_A[next_idx];
        t_A[next_idx] = tmp;
      }
    }
  }

  return;
}

void 
sort_using_bitonic_on_device (std::vector<int>& t_A, 
  const std::size_t& t_num_threads_per_block = 64UL)
{
  const std::size_t m_num_elems = t_A.size(); 

  if (not m_num_elems)
  {
    return;
  }

  if (((m_num_elems) & (m_num_elems - 1UL)) != 0UL)
  {
    std::stringstream msg;
    msg<<"- error: input array size for sorting is not a power of 2"<<std::endl;
    msg<<"- given array size: "<<m_num_elems<<std::endl;

    throw std::invalid_argument(msg.str());
  }

  const auto m_num_threads_per_block = std::min(t_num_threads_per_block, m_num_elems);
  const auto m_num_blocks = m_num_elems / m_num_threads_per_block; 

  if ((m_num_blocks * m_num_threads_per_block) != m_num_elems)
  {
    std::stringstream msg;
    msg<<"- error: number of threads need to be commensurate with the number of elements"<<std::endl;
    msg<<"- number of elements: "<<m_num_elems<<std::endl;
    msg<<"- number of threads:  "<<t_num_threads_per_block<<std::endl;

    throw std::invalid_argument(msg.str());
  }

  cudaError_t m_cuda_error;

  int* m_dA;
  cudaMalloc((void**)&m_dA, m_num_elems * sizeof(int));
  m_cuda_error = cudaGetLastError();
  if (m_cuda_error != cudaSuccess)
  {
    std::cout<<"- CUDA error: "<<cudaGetErrorString(m_cuda_error)<<std::endl;
    std::exit(-1);
  }

  cudaMemcpy(m_dA, t_A.data(), m_num_elems * sizeof(int), cudaMemcpyHostToDevice);
  m_cuda_error = cudaGetLastError();
  if (m_cuda_error != cudaSuccess)
  {
    std::cout<<"- CUDA error: "<<cudaGetErrorString(m_cuda_error)<<std::endl;
    std::exit(-1);
  }

  for (std::size_t h = 2UL; h <= t_A.size(); h <<= 1UL) /// half
  {
    for (std::size_t n = h >> 1UL; n > 0UL; n >>= 1UL)  /// next element id
    {
      complete_one_pass_of_bitonic_sort_on_device<<<m_num_blocks, m_num_threads_per_block>>>(h, n, m_dA);
      m_cuda_error = cudaGetLastError();
      if (m_cuda_error != cudaSuccess)
      {
        std::cout<<"- CUDA error: "<<cudaGetErrorString(m_cuda_error)<<std::endl;
        std::exit(-1);
      }

      cudaDeviceSynchronize();
      m_cuda_error = cudaGetLastError();
      if (m_cuda_error != cudaSuccess)
      {
        std::cout<<"- CUDA error: "<<cudaGetErrorString(m_cuda_error)<<std::endl;
        std::exit(-1);
      }
    }
  }

  cudaMemcpy(t_A.data(), m_dA, m_num_elems * sizeof(int), cudaMemcpyDeviceToHost);
  m_cuda_error = cudaGetLastError();
  if (m_cuda_error != cudaSuccess)
  {
    std::cout<<"- CUDA error: "<<cudaGetErrorString(m_cuda_error)<<std::endl;
    std::exit(-1);
  }

  cudaFree(m_dA);
  m_cuda_error = cudaGetLastError();
  if (m_cuda_error != cudaSuccess)
  {
    std::cout<<"- CUDA error: "<<cudaGetErrorString(m_cuda_error)<<std::endl;
    std::exit(-1);
  }

  return;
}

void 
complete_one_pass_of_bitonic_sort (const std::size_t& t_h, 
  const std::size_t& t_n, 
  std::vector<int>& t_A)
{
  for (std::size_t i = 0; i < t_A.size(); ++i)
  {
    const auto next_idx = i ^ t_n;

    if (next_idx > i)
    {
      if (i & t_h) /// 0: top half, 1: bottom half.
      {
        if (t_A[i] < t_A[next_idx])
        {
          const auto tmp = t_A[i];
          t_A[i] = t_A[next_idx];
          t_A[next_idx] = tmp;
        }
      }
      else
      {
        if (t_A[i] > t_A[next_idx])
        {
          const auto tmp = t_A[i];
          t_A[i] = t_A[next_idx];
          t_A[next_idx] = tmp;
        }
      }
    }
  }

  return;
}

void 
sort_using_bitonic (std::vector<int>& t_A)
{
  for (std::size_t h = 2UL; h <= t_A.size(); h <<= 1UL) /// half
  {
    for (std::size_t n = h >> 1UL; n > 0; n >>= 1UL)  /// next element id
    {
      complete_one_pass_of_bitonic_sort (h, n, t_A);
    }
  }

  return;
}

int 
main (int argc, char **argv)
{
  const std::size_t m_num_elems = 1UL<<11UL;
  std::vector<int> m_elems(m_num_elems, 0);

  for (std::size_t i = 0; i < m_num_elems; ++i)
  {
    m_elems[i] = m_num_elems - 1 - i;
  }

  std::cout<<"- input elements: "<<m_elems<<std::endl;

  cudaError_t m_cuda_error;

  cudaDeviceProp m_dev_prop;
  cudaGetDeviceProperties(&m_dev_prop, 0); /// assumes 1 nvidia device at 0. 
  m_cuda_error = cudaGetLastError();
  if (m_cuda_error != cudaSuccess)
  {
    std::cout<<"- CUDA error: "<<cudaGetErrorString(m_cuda_error)<<std::endl;
    std::exit(-1);
  }

  sort_using_bitonic_on_device (m_elems, m_dev_prop.maxThreadsPerBlock);
  /// @todo: query the number of threads to be used. 

  std::cout<<"- sorted elements (in nondecreasing order): "<<m_elems<<std::endl;

  return 0;
}